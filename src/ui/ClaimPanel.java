/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import table.ClaimTableModel;
import database.Claim;
import database.ClaimDao;
import database.User;
import database.UserDao;
import java.awt.Component;
import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;
import table.UserTableModel;

enum FormStateClaim {
    INIT,
    ADDNEW,
    UPDATE
}

public class ClaimPanel extends javax.swing.JPanel {

    /**
     * Creates new form ClaimPanel
     */
    public ClaimPanel() {
        initComponents();
        updateTable();
        setState(FormStateClaim.INIT);
        addRadioButton();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        formPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        orderlIdText = new javax.swing.JTextField();
        claimIdText = new javax.swing.JTextField();
        dateClaimText = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        detailClaimText = new javax.swing.JTextArea();
        jLabel5 = new javax.swing.JLabel();
        StatusRadioButton = new javax.swing.JRadioButton();
        StatusRadioButton1 = new javax.swing.JRadioButton();
        jPanel4 = new javax.swing.JPanel();
        addButton = new javax.swing.JButton();
        saveButton = new javax.swing.JButton();
        deleteButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        dataTable = new javax.swing.JTable();

        formPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("เลขที่การเคลม :");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("วันที่เคลมสินค้า :");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("รายละเอียดการเคลม :");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("สถานะการเคลม :");

        orderlIdText.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        orderlIdText.setEnabled(false);

        claimIdText.setEditable(false);
        claimIdText.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        dateClaimText.setEditable(false);
        dateClaimText.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        detailClaimText.setColumns(20);
        detailClaimText.setRows(5);
        jScrollPane1.setViewportView(detailClaimText);

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("เลขการซื้อที่สินค้า :");

        StatusRadioButton.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        StatusRadioButton.setText("สำเร็จ");

        StatusRadioButton1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        StatusRadioButton1.setText("ไม่สำเร็จ");

        javax.swing.GroupLayout formPanelLayout = new javax.swing.GroupLayout(formPanel);
        formPanel.setLayout(formPanelLayout);
        formPanelLayout.setHorizontalGroup(
            formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(formPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(formPanelLayout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(claimIdText, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(formPanelLayout.createSequentialGroup()
                        .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(formPanelLayout.createSequentialGroup()
                                .addComponent(dateClaimText, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel3))
                            .addGroup(formPanelLayout.createSequentialGroup()
                                .addComponent(StatusRadioButton)
                                .addGap(18, 18, 18)
                                .addComponent(StatusRadioButton1)))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(orderlIdText, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        formPanelLayout.setVerticalGroup(
            formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(formPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(claimIdText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(orderlIdText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(formPanelLayout.createSequentialGroup()
                        .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(dateClaimText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(StatusRadioButton)
                            .addComponent(StatusRadioButton1)))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        addButton.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        addButton.setText("เพิ่ม");
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });

        saveButton.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        saveButton.setText("บันทึก");
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });

        deleteButton.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        deleteButton.setText("ลบ");
        deleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteButtonActionPerformed(evt);
            }
        });

        cancelButton.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cancelButton.setText("ยกเลิก");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(addButton, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(saveButton, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(deleteButton, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(cancelButton, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addButton)
                    .addComponent(saveButton)
                    .addComponent(deleteButton)
                    .addComponent(cancelButton))
                .addContainerGap(15, Short.MAX_VALUE))
        );

        jScrollPane2.setBorder(null);

        dataTable.setModel(claimTableModel);
        dataTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                dataTableMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(dataTable);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(formPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane2))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(formPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        formToObject();
        if (checkInput()) {
            ClaimDao.save(claim);
            updateTable();
            clearForm();
            setState(FormStateClaim.INIT);
        }

    }//GEN-LAST:event_saveButtonActionPerformed

    private void dataTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dataTableMouseClicked
        setState(FormStateClaim.UPDATE);
        // หา id
        String idStr = dataTable.getValueAt(dataTable.getSelectedRow(), 0).toString();
        int id = Integer.parseInt(idStr);
        // ดึง user จาก db ด้วย id
        claim = ClaimDao.getClaims(id);
        // เอา user ไปใส่ form
        toForm(claim);
    }//GEN-LAST:event_dataTableMouseClicked

    private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed
        setState(FormStateClaim.ADDNEW);
        claim = new Claim();
    }//GEN-LAST:event_addButtonActionPerformed

    private void deleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteButtonActionPerformed
        ClaimDao.delete(claim);
        updateTable();
        clearForm();
        setState(FormStateClaim.INIT);
    }//GEN-LAST:event_deleteButtonActionPerformed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        clearForm();
        setState(FormStateClaim.INIT);
    }//GEN-LAST:event_cancelButtonActionPerformed

    private Claim claim;
    ClaimTableModel claimTableModel = new ClaimTableModel();
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton StatusRadioButton;
    private javax.swing.JRadioButton StatusRadioButton1;
    private javax.swing.JButton addButton;
    private javax.swing.JButton cancelButton;
    private javax.swing.JTextField claimIdText;
    private javax.swing.JTable dataTable;
    private javax.swing.JTextField dateClaimText;
    private javax.swing.JButton deleteButton;
    private javax.swing.JTextArea detailClaimText;
    private javax.swing.JPanel formPanel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField orderlIdText;
    private javax.swing.JButton saveButton;
    // End of variables declaration//GEN-END:variables

    private void updateTable() {
        claimTableModel.setData(ClaimDao.getClaims());
    }

    private void setState(FormStateClaim state) {
        switch (state) {
            case INIT:
                addButton.setEnabled(true);
                saveButton.setEnabled(false);
                deleteButton.setEnabled(false);
                cancelButton.setEnabled(false);
                setFormState(false);
                break;
            case ADDNEW:
                addButton.setEnabled(false);
                saveButton.setEnabled(true);
                deleteButton.setEnabled(false);
                cancelButton.setEnabled(true);
                setFormState(true);
                break;
            case UPDATE:
                addButton.setEnabled(false);
                saveButton.setEnabled(true);
                deleteButton.setEnabled(true);
                cancelButton.setEnabled(true);
                setFormState(true);
                break;
        }
    }

    private void setFormState(boolean state) {
        for (Component c : formPanel.getComponents()) {
            c.setEnabled(state);
        }
    }

    private void formToObject() {
        int status = 0;
        if (StatusRadioButton.isSelected()) {
            status = 0;
        } else if (StatusRadioButton1.isSelected()) {
            status = 1;
        }
        claim.setDetailClaim(detailClaimText.getText());
        claim.setStatusClaim(status);
        if (orderlIdText.getText().trim().equals("")) {
            claim.setOrderId(1);
        } else {
            claim.setOrderId(Integer.parseInt(orderlIdText.getText()));
        }
    }

    private void clearForm() {
        claimIdText.setText("");
        orderlIdText.setText("");
        dateClaimText.setText("");
        detailClaimText.setText("");
        StatusRadioButton1.setSelected(false);
        StatusRadioButton.setSelected(false);
    }

    private void toForm(Claim claim) {
        claimIdText.setText("" + claim.getClaimId());
        dateClaimText.setText(claim.getDateClaim());
        orderlIdText.setText("" + claim.getOrderId());
        detailClaimText.setText(claim.getDetailClaim());
        if (claim.getStatusClaim() == 0) {
            StatusRadioButton.setSelected(true);
            StatusRadioButton1.setSelected(false);
        } else {
            StatusRadioButton1.setSelected(true);
            StatusRadioButton.setSelected(false);
        }
    }

    private void addRadioButton() {
        ButtonGroup G = new ButtonGroup();
        G.add(StatusRadioButton);
        G.add(StatusRadioButton1);
    }

    public boolean checkInput() {
        if (claim.getDetailClaim().trim().equals("") || Integer.toString(claim.getOrderId()).trim().equals("")) {
            JOptionPane.showMessageDialog(null, "กรุณาใส่ขอ้มูลให้ครบ !!", "Error", JOptionPane.WARNING_MESSAGE);
            return false;
        } else if (!StatusRadioButton.isSelected() && !StatusRadioButton1.isSelected()) {
            JOptionPane.showMessageDialog(null, "กรุณาติ๊กสถานะการเคลม !!", "Error", JOptionPane.WARNING_MESSAGE);
            return false;
        }
        return true;
    }

}
