/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import database.Database;
import database.Order;
import database.Product;
import database.ProductDao;
import database.UserDao;
import database.User;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author O-Live
 */
public class OrderDetailDao {
    
    public static int insert(Order order) {
       int id = -1;
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "INSERT INTO OrderDetail (\n"
                    + "                        total,\n"
                    + "                        productId,\n"            
                    + "                        orderId\n"
                    + "                    )\n"
                    + "                    VALUES (\n"
                    + "                        %d,\n"
                    + "                        %d,\n"
                    + "                        %d\n"
                    + "                    );";
            stm.execute(String.format(sql,order.getTotal(),order.getProductId(),order.getOrderId()));
        ResultSet rs = stm.getGeneratedKeys();
        if(rs.next()) {
             id = rs.getInt("last_insert_rowid()");
        }
        Database.close();
        return id;
        } catch (SQLException ex) {
            Logger.getLogger(OrderDetailDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return id;
    }
    public static boolean update(Order order) {
        Connection conn = Database.connect();
        String sql = "UPDATE OrderDetail SET \n"
                + " total=%d,\n"
                + " productId=%d,\n"            
                + " orderId=%d\n"
                + "WHERE orderDetailId = %d;";
        Statement stm;
        try {
            stm = conn.createStatement();
            boolean re = stm.execute(String.format(sql,order.getTotal(), order.getOrderDetailId()));
            Database.close();
            return re;
        } catch (SQLException ex) {
            Logger.getLogger(OrderDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
        return false;
    }
    
    public static boolean delete(Order order) {
        String sql = "DELETE FROM Product WHERE orderDetailId = %d;";
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            boolean re = stm.execute(String.format(sql, order.getOrderDetailId()));
            return re;
        } catch (SQLException ex) {
            Logger.getLogger(OrderDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
        return true;
    }

    public static ArrayList<Order> getOrders() {
        ArrayList<Order> list = new ArrayList();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT orderId,\n"
                    + "       orderDetailId,\n"
                    + "       customerId,\n"
                    + "       nameProduct,\n"
                    + "       priceProduct,\n"
                    + "       quantityProduct,\n"
                    + "       productId,\n"
                    + "       total,\n"
                    + "       userId"
                    + "   FROM Order";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                System.out.println(rs.getInt("orderDetailId") + rs.getString("nameProduct"));
                Order order = toObject(rs);
                list.add(order);

            }
            Database.close();
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(OrderDetailDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }

    private static Order toObject(ResultSet rs) throws SQLException {
        Order order;
        order = new Order();
        order.setProductId(rs.getInt("orderId"));
        order.setProductId(rs.getInt("orderDetailId"));
        order.setNameProduct(rs.getString("customerId"));
        order.setNameProduct(rs.getString("nameProduct"));
        order.setNameProduct(rs.getString("priceProduct"));
        order.setNameProduct(rs.getString("quantityProduct"));
        order.setNameProduct(rs.getString("productId"));
        order.setNameProduct(rs.getString("total"));
        order.setNameProduct(rs.getString("userId"));
    
        return order;
    }

    public static Order getOrder(int orderId) {
        String sql = "SELECT * FROM Order WHERE orderDetailId = %d";
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql, orderId));
            if (rs.next()) {
                Order order = toObject(rs);
                Database.close();
                return order;
            }
        } catch (SQLException ex) {
            Logger.getLogger(OrderDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    

    public static void save(Order order) {
        if (order.getOrderDetailId() < 0) {
            insert(order);
        } else {
            update(order);
        }
    }
}
