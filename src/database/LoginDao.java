/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author O-Live
 */
public class LoginDao {

    public static boolean insert(Login login) {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "INSERT INTO Login (\n"
                    + "			userId\n"
                    + "                  )\n"
                    + "                 VALUES (\n"
                    + "                     %d\n"
                    + "                     );";
            stm.execute(String.format(sql, login.getUserId()));
            Database.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
        return true;
    }

    public static void save(Login login) {
         insert(login);
    }

    public static Login getUsers(int userId) {
        String sql = "SELECT * FROM Login WHERE userId = %d";
        Connection conn = Database.connect();

        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql, userId));
            if (rs.next()) {
                Login login = toObject(rs);
                Database.close();
                return login;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }
    
    private static Login toObject(ResultSet rs) throws SQLException {
        Login login;
        login = new Login();
        login.setLoginId(rs.getInt("loginId"));
        login.setUserId(rs.getInt("userId"));
        return login;
    }

}
