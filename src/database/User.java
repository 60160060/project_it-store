/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

/**
 *
 * @author BB
 */
public class User {

    int userId;
    String usernameUser;
    String passwordUser;
    String firstnameUser;
    String lastnameUser;
    String telUser;
    String addressUser;
    String idCardUser;
    int typeUser;

    public User() {
        this.userId = -1;
    }

    public User(int userId, String usernameUser, String firstnameUser, String lastnameUser, String telUser, String addressUser, String idCardUser, int typeUser) {
        this.userId = userId;
        this.usernameUser = usernameUser;
        this.firstnameUser = firstnameUser;
        this.lastnameUser = lastnameUser;
        this.telUser = telUser;
        this.addressUser = addressUser;
        this.idCardUser = idCardUser;
        this.typeUser = typeUser;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsernameUser() {
        return usernameUser;
    }

    public void setUsernameUser(String usernameUser) {
        this.usernameUser = usernameUser;
    }

    public String getPasswordUser() {
        return passwordUser;
    }

    public void setPasswordUser(String passwordUser) {
        this.passwordUser = passwordUser;
    }

    public String getFirstnameUser() {
        return firstnameUser;
    }

    public void setFirstnameUser(String firstnameUser) {
        this.firstnameUser = firstnameUser;
    }

    public String getLastnameUser() {
        return lastnameUser;
    }

    public void setLastnameUser(String lastnameUser) {
        this.lastnameUser = lastnameUser;
    }

    public String getTelUser() {
        return telUser;
    }

    public void setTelUser(String telUser) {
        this.telUser = telUser;
    }

    public String getAddressUser() {
        return addressUser;
    }

    public void setAddressUser(String addressUser) {
        this.addressUser = addressUser;
    }

    public String getIdCardUser() {
        return idCardUser;
    }

    public void setIdCardUser(String idCardUser) {
        this.idCardUser = idCardUser;
    }

    public int getTypeUser() {
        return typeUser;
    }

    public void setTypeUser(int typeUser) {
        this.typeUser = typeUser;
    }

    @Override
    public String toString() {
        return "User{" + "userId=" + userId + ", usernameUser=" + usernameUser + ", passwordUser=" + passwordUser + ", firstnameUser=" + firstnameUser + ", lastnameUser=" + lastnameUser + ", telUser=" + telUser + ", addressUser=" + addressUser + ", idCardUser=" + idCardUser + ", typeUser=" + typeUser + '}';
    }
    
    

}
