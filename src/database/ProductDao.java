/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author O-Live
 */
public class ProductDao {

    public static boolean insert(Product product) {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "INSERT INTO Product (\n"
                    + "                        nameProduct,\n"
                    + "                        typeProduct,\n"
                    + "                        detailProduct,\n"
                    + "                        priceProduct,\n"
                    + "                        quantityProduct,\n"
                    + "                        userId\n"
                    + "                    )\n"
                    + "                    VALUES (\n"
                    + "                        '%s',\n"
                    + "                        '%s',\n"
                    + "                        '%s',\n"
                    + "                        %d,\n"
                    + "                        %d,\n"
                    + "                        %d\n"
                    + "                    );";
            stm.execute(String.format(sql,
                    product.getNameProduct(),
                    product.getTypeProduct(),
                    product.getDetailProduct(),
                    product.getPriceProduct(),
                    product.getQuantityProduct(),
                    product.getUserId()));
            Database.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return true;
    }

    public static boolean update(Product product) {
        Connection conn = Database.connect();
        String sql = "UPDATE Product SET \n"
                + " nameProduct = '%s',\n"
                + " typeProduct = '%s',\n"
                + " detailProduct = '%s',\n"
                + " priceProduct = %d,\n"
                + " quantityProduct = %d,\n"
                + " userId = %d\n"
                + "WHERE productId = %d;";
        Statement stm;
        try {
            stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql,
                    product.getNameProduct(),
                    product.getTypeProduct(),
                    product.getDetailProduct(),
                    product.getPriceProduct(),
                    product.getQuantityProduct(),
                    product.getUserId(),
                    product.getProductId()
            ));
            Database.close();
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
        return false;
    }

    public static boolean delete(Product product) {
        String sql = "DELETE FROM Product WHERE productId = %d;";
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql, product.getProductId()));
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
        return true;
    }

    public static ArrayList<Product> getProducts() {
        ArrayList<Product> list = new ArrayList();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT productId,\n"
                    + "       nameProduct,\n"
                    + "       typeProduct,\n"
                    + "       detailProduct,\n"
                    + "       priceProduct,\n"
                    + "       quantityProduct,\n"
                    + "       userId"
                    + "   FROM Product";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                System.out.println(rs.getInt("productId") + " " + rs.getString("nameProduct"));
                Product product = toObject(rs);
                list.add(product);

            }
            Database.close();
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }

    private static Product toObject(ResultSet rs) throws SQLException {
        Product product;
        product = new Product();
        product.setProductId(rs.getInt("productId"));
        product.setNameProduct(rs.getString("nameProduct"));
        product.setTypeProduct(rs.getString("typeProduct"));
        product.setDetailProduct(rs.getString("detailProduct"));
        product.setPriceProduct(rs.getInt("priceProduct"));
        product.setQuantityProduct(rs.getInt("quantityProduct"));
        product.setUserId(rs.getInt("userId"));
        return product;
    }

    public static Product getProduct(int productId) {
        String sql = "SELECT * FROM Product WHERE productId = %d";
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql, productId));
            if (rs.next()) {
                Product product = toObject(rs);
                Database.close();
                return product;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static void save(Product product) {
        if (product.getProductId() < 0) {
            insert(product);
        } else {
            update(product);
        }
    }
}
