/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

/**
 *
 * @author BB
 */
public class Customer {

    int customerId;
    String firstnameCustomer;
    String lastnameCustomer;
    String telCustomer;
    String emailCustomer;
    int userId;

    public Customer() {
        this.customerId = -1;
    }

    public Customer(int customerId, String firstnameCustomer, String lastnameCustomer, String telCustomer, String emailCustomer, int userId) {
        this.customerId = customerId;
        this.firstnameCustomer = firstnameCustomer;
        this.lastnameCustomer = lastnameCustomer;
        this.telCustomer = telCustomer;
        this.emailCustomer = emailCustomer;
        this.userId = userId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getFirstnameCustomer() {
        return firstnameCustomer;
    }

    public void setFirstnameCustomer(String firstnameCustomer) {
        this.firstnameCustomer = firstnameCustomer;
    }

    public String getLastnameCustomer() {
        return lastnameCustomer;
    }

    public void setLastnameCustomer(String lastnameCustomer) {
        this.lastnameCustomer = lastnameCustomer;
    }

    public String getTelCustomer() {
        return telCustomer;
    }

    public void setTelCustomer(String telCustomer) {
        this.telCustomer = telCustomer;
    }

    public String getEmailCustomer() {
        return emailCustomer;
    }

    public void setEmailCustomer(String emailCustomer) {
        this.emailCustomer = emailCustomer;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Customer{" + "customerId=" + customerId + ", firstnameCustomer=" + firstnameCustomer + ", lastnameCustomer=" + lastnameCustomer + ", telCustomer=" + telCustomer + ", emailCustomer=" + emailCustomer + ", userId=" + userId + '}';
    }

}
