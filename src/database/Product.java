/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

/**
 *
 * @author KhunMarius
 */
public class Product {
    int productId;
    String nameProduct;
    String typeProduct;
    String detailProduct;
    int priceProduct;
    int quantityProduct;
    int userId;

    public Product() {
        this.productId = -1;
        
    }

    public Product(int productId, String nameProduct, String typeProduct, String detailProduct, int priceProduct, int quantityProduct, int userId) {
        this.productId = productId;
        this.nameProduct = nameProduct;
        this.typeProduct = typeProduct;
        this.detailProduct = detailProduct;
        this.priceProduct = priceProduct;
        this.quantityProduct = quantityProduct;
        this.userId = userId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getNameProduct() {
        return nameProduct;
    }

    public void setNameProduct(String nameProduct) {
        this.nameProduct = nameProduct;
    }

    public String getTypeProduct() {
        return typeProduct;
    }

    public void setTypeProduct(String typeProduct) {
        this.typeProduct = typeProduct;
    }

    public String getDetailProduct() {
        return detailProduct;
    }

    public void setDetailProduct(String detailProduct) {
        this.detailProduct = detailProduct;
    }

    public int getPriceProduct() {
        return priceProduct;
    }

    public void setPriceProduct(int priceProduct) {
        this.priceProduct = priceProduct;
    }

    public int getQuantityProduct() {
        return quantityProduct;
    }

    public void setQuantityProduct(int quantityProduct) {
        this.quantityProduct = quantityProduct;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Product{" + "productId=" + productId + ", nameProduct=" + nameProduct + ", typeProduct=" + typeProduct + ", detailProduct=" + detailProduct + ", priceProduct=" + priceProduct + ", quantityProduct=" + quantityProduct + ", userId=" + userId + '}';
    }
        
}
