/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author O-Live
 */
public class CustomerDao {

    public static boolean insert(Customer customer) {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "INSERT INTO Customer (   \n"
                    + "			firstnameCustomer,     \n"
                    + "			lastnameCustomer,    \n"
                    + "			telCustomer,   \n"
                    + "			emailCustomer,    \n"
                    + "			userId\n"
                    + "                     )\n"
                    + "                     VALUES (\n"
                    + "			'%s',\n"
                    + "			'%s',\n"
                    + "			'%s',\n"
                    + "			'%s',\n"
                    + "			%d\n"
                    + "			 );";
            stm.execute(String.format(sql, customer.getFirstnameCustomer(), customer.getLastnameCustomer(),
                    customer.getTelCustomer(), customer.getEmailCustomer(), customer.getUserId()));
            Database.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
        return true;
    }

    public static boolean update(Customer customer) {
        String sql = "UPDATE Customer\n"
                + "   SET firstnameCustomer = '%s',\n"
                + "       lastnameCustomer = '%s',\n"
                + "       telCustomer = '%s',\n"
                + "       emailCustomer = '%s',\n"
                + "       userId = '%d'\n"
                + " WHERE customerId = %d;";

        Connection conn = Database.connect();
        Statement stm;
        try {
            stm = conn.createStatement();
            boolean re = stm.execute(String.format(sql, customer.getFirstnameCustomer(), customer.getLastnameCustomer(),
                    customer.getTelCustomer(), customer.getEmailCustomer(), customer.getUserId(),customer.getCustomerId()));
            Database.close();
            return re;
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
        return false;
    }

    public static boolean delete(Customer customer) {
        String sql = "DELETE FROM Customer WHERE customerId = %d ;";
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            boolean re = stm.execute(String.format(sql, customer.getCustomerId()));
            Database.close();
            return re;
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return false;
    }

    public static ArrayList<Customer> getCustomers() {
        ArrayList<Customer> list = new ArrayList();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT customerId,\n"
                    + "       firstnameCustomer,\n"
                    + "       lastnameCustomer,\n"
                    + "       telCustomer,\n"
                    + "       emailCustomer,\n"
                    + "       userId\n"
                    + "  FROM Customer";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                System.out.println(rs.getInt("customerId") + " " + rs.getString("firstnameCustomer"));
                Customer customer = toObject(rs);
                list.add(customer);
            }
            Database.close();
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
        return null;
    }

    public static Customer getCustomers(int customerId) {
        String sql = "SELECT * FROM Customer WHERE customerId = %d";
        Connection conn = Database.connect();

        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql, customerId));
            if (rs.next()) {
                Customer customer = toObject(rs);
                Database.close();
                return customer;
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }

    private static Customer toObject(ResultSet rs) throws SQLException {
        Customer customer;
        customer = new Customer();
        customer.setCustomerId(rs.getInt("customerId"));
        customer.setFirstnameCustomer(rs.getString("firstnameCustomer"));
        customer.setLastnameCustomer(rs.getString("lastnameCustomer"));
        customer.setTelCustomer(rs.getString("telCustomer"));
        customer.setEmailCustomer(rs.getString("emailCustomer"));
        customer.setUserId(rs.getInt("userId"));
        return customer;
    }

    public static void save(Customer customer) {
        if (customer.getCustomerId() < 0) {
            insert(customer);
        } else {
            update(customer);
        }
    }
}
