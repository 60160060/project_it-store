/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

/**
 *
 * @author VAFFLE
 */
public class Order {
    int orderId;
    int orderDetailId;
    int customerId;
    String nameProduct;
    int priceProduct;
    int quantityOrder;
    int productId;
    int total;
    int userId;
    

    public Order() {
        this.orderId = -1;
        
    }

    public Order(int orderId,int orderDetailId, String nameProduct, String customerId, int priceProduct, int quantityOrder,int productId,int total, int userId) {
        this.orderId = orderId;
        this.orderDetailId = orderDetailId;
        this.nameProduct = nameProduct;
        this.priceProduct = priceProduct;
        this.quantityOrder = quantityOrder;
        this.productId = productId;
        this.userId = userId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }
    
     public int getOrderDetailId() {
        return orderDetailId;
    }

    public void setOrderDetailId(int orderDetailId) {
        this.orderDetailId = orderDetailId;
    }
    
     public int getCustomerId() {
        return orderId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getNameProduct() {
        return nameProduct;
    }

    public void setNameProduct(String nameProduct) {
        this.nameProduct = nameProduct;
    }
    
    public int getPriceProduct() {
        return priceProduct;
    }

    public void setPriceProduct(int priceProduct) {
        this.priceProduct = priceProduct;
    }

    public int getQuantityOrder() {
        return quantityOrder;
    }

    public void setQuantityOrder(int quantityOrder) {
        this.quantityOrder = quantityOrder;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

     public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
    
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Order{" + "productId=" + productId + ", nameProduct=" + nameProduct + ", priceProduct=" + priceProduct + ", quantityProduct=" + quantityOrder +", productId=" + productId + ", userId=" + userId + '}';
    }
        
}
