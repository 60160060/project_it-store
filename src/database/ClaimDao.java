/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import database.Database;
import static database.CustomerDao.insert;
import static database.UserDao.insert;
import static database.UserDao.update;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author O-Live
 */
public class ClaimDao {

    public static boolean insert(Claim claim) {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "INSERT INTO Claim (\n"
                    + "detailClaim,\n"
                    + "statusClaim,\n"
                    + "orderId\n"
                    + "                  )\n"
                    + "VALUES (\n"
                    + "'%s',\n"
                    + "%d,\n"
                    + "%d\n"
                    + ");";
            stm.execute(String.format(sql, claim.getDetailClaim(), claim.getStatusClaim(), claim.getOrderId()));
            Database.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ClaimDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
        return true;
    }

    public static boolean update(Claim claim) {
        String sql = "UPDATE Claim SET \n"
                + "detailClaim = '%s',\n"
                + "statusClaim = '%d',\n"
                + "orderId = '%d'\n"
                + "WHERE claimId = %d;\n"
                + "";

        Connection conn = Database.connect();
        Statement stm;
        try {
            stm = conn.createStatement();
            boolean re = stm.execute(String.format(sql, claim.getDetailClaim(), claim.getStatusClaim(),
                    claim.getOrderId(), claim.getClaimId()));
            Database.close();
            return re;
        } catch (SQLException ex) {
            Logger.getLogger(ClaimDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
        return false;
    }

    public static boolean delete(Claim claim) {
        String sql = "DELETE FROM Claim WHERE claimId = %d ;";
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            boolean re = stm.execute(String.format(sql, claim.getClaimId()));
            Database.close();
            return re;
        } catch (SQLException ex) {
            Logger.getLogger(ClaimDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return false;
    }

    public static ArrayList<Claim> getClaims() {
        ArrayList<Claim> list = new ArrayList();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT claimId,\n"
                    + "       dateClaim,\n"
                    + "       detailClaim,\n"
                    + "       statusClaim,\n"
                    + "       orderId\n"
                    + "  FROM Claim;";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                System.out.println(rs.getInt("claimId") + " " + rs.getString("detailClaim"));
                Claim claim = toObject(rs);
                list.add(claim);
            }
            Database.close();
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(ClaimDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
        return null;
    }

    public static Claim getClaims(int claimId) {
        String sql = "SELECT * FROM Claim WHERE claimId = %d;";
        Connection conn = Database.connect();

        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql, claimId));
            if (rs.next()) {
                Claim claim = toObject(rs);
                Database.close();
                return claim;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ClaimDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }

    private static Claim toObject(ResultSet rs) throws SQLException {
        Claim claim;
        claim = new Claim();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String DateClaim = dateFormat.format(new Date());
        claim.setClaimId(rs.getInt("ClaimId"));
        claim.setDateClaim(DateClaim);
        claim.setDetailClaim(rs.getString("DetailClaim"));
        claim.setStatusClaim(rs.getInt("StatusClaim"));
        claim.setOrderId(rs.getInt("OrderId"));
        return claim;
    }
    
    public static void save(Claim claim) {
        if (claim.getClaimId()< 0) {
            insert(claim);
        } else {
            update(claim);
        }
    }

}
