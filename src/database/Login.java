/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

/**
 *
 * @author BB
 */
public class Login {

    int loginId;
    String loginTime;
    int userId;

    public Login() {
        loginId = -1;
    }

    public Login(int loginId, String loginTime, int userId) {
        this.loginId = loginId;
        this.loginTime = loginTime;
        this.userId = userId;
    }

    public int getLoginId() {
        return loginId;
    }

    public void setLoginId(int loginId) {
        this.loginId = loginId;
    }

    public String getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(String loginTime) {
        this.loginTime = loginTime;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Login{" + "loginId=" + loginId + ", loginTime=" + loginTime + ", userId=" + userId + '}';
    }

}
