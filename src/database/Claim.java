/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

/**
 *
 * @author BB
 */
public class Claim {

    int claimId;
    String dateClaim;
    String detailClaim;
    int statusClaim;
    int orderId;

    public Claim() {
        claimId = -1;
    }

    public Claim(int claimId, String dateClaim, String detailClaim, int statusClaim, int orderId) {
        this.claimId = claimId;
        this.dateClaim = dateClaim;
        this.detailClaim = detailClaim;
        this.statusClaim = statusClaim;
        this.orderId = orderId;
    }

    public int getClaimId() {
        return claimId;
    }

    public void setClaimId(int claimId) {
        this.claimId = claimId;
    }

    public String getDateClaim() {
        return dateClaim;
    }

    public void setDateClaim(String dateClaim) {
        this.dateClaim = dateClaim;
    }

    public String getDetailClaim() {
        return detailClaim;
    }

    public void setDetailClaim(String detailClaim) {
        this.detailClaim = detailClaim;
    }

    public int getStatusClaim() {
        return statusClaim;
    }

    public void setStatusClaim(int statusClaim) {
        this.statusClaim = statusClaim;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    @Override
    public String toString() {
        return "Claim{" + "claimId=" + claimId + ", dateClaim=" + dateClaim + ", detailClaim=" + detailClaim + ", statusClaim=" + statusClaim + ", orderId=" + orderId + '}';
    }

}
