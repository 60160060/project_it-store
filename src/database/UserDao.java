/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author O-Live
 */
public class UserDao {

    public static boolean insert(User user) {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "INSERT INTO User (\n"
                    + "                    usernameUser,\n"
                    + "                    passwordUser,\n"
                    + "                    firstnameUser,\n"
                    + "                    lastnameUser,\n"
                    + "                    telUser,\n"
                    + "                    addressUser,\n"
                    + "                    idCardUser,\n"
                    + "                    typeUser\n"
                    + "                  )\n"
                    + "                 VALUES (\n"
                    + "                     '%s',\n"
                    + "                     '%s',\n"
                    + "                     '%s',\n"
                    + "                     '%s',\n"
                    + "                     '%s',\n"
                    + "                     '%s',\n"
                    + "                     '%s',\n"
                    + "                     %d\n"
                    + "                     );";
            stm.execute(String.format(sql, user.getUsernameUser(), user.getPasswordUser(),
                    user.getFirstnameUser(), user.getLastnameUser(), user.getTelUser(),
                    user.getAddressUser(), user.getIdCardUser(), user.getTypeUser()));
            Database.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
        return true;
    }

    public static boolean update(User user) {
        String sql = "UPDATE User SET\n"
                + "	usernameUser = '%s',\n"
                + "	passwordUser = '%s',\n"
                + "	firstnameUser = '%s',\n"
                + "	lastnameUser = '%s',\n"
                + "	telUser = '%s',\n"
                + "	addressUser = '%s',\n"
                + "	idCardUser = '%s',\n"
                + "	typeUser = %d\n"
                + "WHERE  userId = %d ;";

        Connection conn = Database.connect();
        Statement stm;
        try {
            stm = conn.createStatement();
            boolean re = stm.execute(String.format(sql, user.getUsernameUser(), user.getPasswordUser(),
                    user.getFirstnameUser(), user.getLastnameUser(), user.getTelUser(),
                    user.getAddressUser(), user.getIdCardUser(), user.getTypeUser(), user.getUserId()));
            Database.close();
            return re;
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
        return false;
    }

    public static boolean delete(User user) {
        String sql = "DELETE FROM User WHERE  userId = %d ;";
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            boolean re = stm.execute(String.format(sql, user.getUserId()));
            Database.close();
            return re;
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return false;
    }

    public static ArrayList<User> getUsers() {
        ArrayList<User> list = new ArrayList();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT userId,\n"
                    + "       usernameUser,\n"
                    + "       passwordUser,\n"
                    + "       firstnameUser,\n"
                    + "       lastnameUser,\n"
                    + "       telUser,\n"
                    + "       addressUser,\n"
                    + "       idCardUser,\n"
                    + "       typeUser\n"
                    + "  FROM User";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                System.out.println(rs.getInt("userId") + " " + rs.getString("usernameUser"));
                User user = toObject(rs);
                list.add(user);
            }
            Database.close();
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
        return null;
    }

    public static User getUsers(int userId) {
        String sql = "SELECT * FROM User WHERE userId = %d";
        Connection conn = Database.connect();

        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql, userId));
            if (rs.next()) {
                User user = toObject(rs);
                Database.close();
                return user;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }

    private static User toObject(ResultSet rs) throws SQLException {
        User user;
        user = new User();
        user.setUserId(rs.getInt("userId"));
        user.setUsernameUser(rs.getString("usernameUser"));
        user.setPasswordUser(rs.getString("passwordUser"));
        user.setFirstnameUser(rs.getString("firstnameUser"));
        user.setLastnameUser(rs.getString("lastnameUser"));
        user.setTelUser(rs.getString("telUser"));
        user.setAddressUser(rs.getString("addressUser"));
        user.setIdCardUser(rs.getString("idCardUser"));
        user.setTypeUser(rs.getInt("typeUser"));
        return user;
    }

    public static void save(User user) {
        if (user.getUserId() < 0) {
            insert(user);
        } else {
            update(user);
        }
    }

}
