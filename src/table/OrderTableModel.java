/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table;

import database.Order;
import database.Product;
import database.ProductDao;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
/**
 *
 * @author informatics
 */
public class OrderTableModel extends AbstractTableModel {
    
    ArrayList<Order> orderList = new ArrayList<Order>();
    String[] columnNames = {"ชื่อสินค้า", "จำนวน","ราคา"};

    @Override
    public int getRowCount() {
        return orderList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
   public Object getValueAt(int rowIndex, int columnIndex) {
        Order order = orderList.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return order.getNameProduct();
            case 1:
                return order.getQuantityOrder();
            case 2:
                return order.getPriceProduct();    
        }
        return "";
    }
    public void setData(ArrayList<Order> userList) {
        this.orderList = userList;
        fireTableDataChanged();
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }
    
}
