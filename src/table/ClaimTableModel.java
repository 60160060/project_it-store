/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table;

import database.Claim;
import database.User;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author BB
 */
public class ClaimTableModel extends AbstractTableModel {

    ArrayList<Claim> claimList = new ArrayList<Claim>();
    String[] columnNames = {"เลขที่การเคลม", "วันที่เคลม", "รายละเอียดการเคลม", "สถานะการเคลม", "เลขที่การชื้อสินค้า"};

    @Override
    public int getRowCount() {
        return claimList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Claim claim = claimList.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return claim.getClaimId();
            case 1:
                return claim.getDateClaim();
            case 2:
                return claim.getDetailClaim();
            case 3:
                if (claim.getStatusClaim() == 0) {
                    return "สำเร็จ";
                } else {
                    return "ไม่สำเร็จ";
                }
            case 4:
                return claim.getOrderId();
        }
        return "";
    }
    
    public void setData(ArrayList<Claim> claimList) {
        this.claimList = claimList;
        fireTableDataChanged();
    }
    
    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }
}
