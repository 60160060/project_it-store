/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table;

import database.Product;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
/**
 *
 * @author informatics
 */
public class ProductTableModel extends AbstractTableModel {
    
    ArrayList<Product> productList = new ArrayList<Product>();
    String[] columnNames = {"เลขที่สินค้า", "ชื่อสินค้า", "ชนิดสินค้า", "รายละเอียด", "ราคา", "จำนวน"};

    @Override
    public int getRowCount() {
        return productList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
   public Object getValueAt(int rowIndex, int columnIndex) {
        Product product = productList.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return product.getProductId();
            case 1:
                return product.getNameProduct();
            case 2:
                return product.getTypeProduct();
            case 3:
                return product.getDetailProduct();
            case 4:
                return product.getPriceProduct();
            case 5:
                return product.getQuantityProduct();      
        }
        return "";
    }
    public void setData(ArrayList<Product> userList) {
        this.productList = userList;
        fireTableDataChanged();
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }
    
}
