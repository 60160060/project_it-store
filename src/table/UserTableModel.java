/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table;

import database.User;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author BB
 */
public class UserTableModel extends AbstractTableModel {

    ArrayList<User> userList = new ArrayList<User>();
    String[] columnNames = {"เลขที่พนักงาน", "ชื่อ", "นามสกุล", "เบอร์โทร", "ตำแหน่ง"};

    @Override
    public int getRowCount() {
        return userList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        User user = userList.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return user.getUserId();
            case 1:
                return user.getFirstnameUser();
            case 2:
                return user.getLastnameUser();
            case 3:
                return user.getTelUser();
            case 4:
                if (user.getTypeUser() == 0) {
                    return "เจ้าของร้าน";
                } else {
                    return "หนักงาน";
                }
        }
        return "";
    }

    public void setData(ArrayList<User> userList) {
        this.userList = userList;
        fireTableDataChanged();
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

}
