/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table;

import database.Customer;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author BB
 */
public class CustomerTableModel extends AbstractTableModel {

    ArrayList<Customer> customerList = new ArrayList<Customer>();
    String[] columnNames = {"เลขที่ลูกค้า", "ชื่อ", "นามสกุล", "เบอร์โทร", "อีเมลล์"};

    @Override
    public int getRowCount() {
        return customerList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Customer customer = customerList.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return customer.getCustomerId();
            case 1:
                return customer.getFirstnameCustomer();
            case 2:
                return customer.getLastnameCustomer();
            case 3:
                return customer.getTelCustomer();
            case 4:
                return customer.getEmailCustomer();
        }
        return "";
    }

    public void setData(ArrayList<Customer> customerList) {
        this.customerList = customerList;
        fireTableDataChanged();
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

}
